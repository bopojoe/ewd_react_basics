import React from "react";
import "../../../node_modules/bootstrap/dist/css/bootstrap.css";

const exerciseTwo = () => {
  const header = "MSc Enterprise Software Systems"
  class CourseModule {
  
  constructor(title, numLectures, numPracticals) {
    this.title = title;
    this.lectures = (typeof numLectures === "undefined") ? "No Lectures" : (numLectures === 1) ? `${numLectures} Lecture` : `${numLectures} Lectures`;
    this.practicals = (typeof numPracticals === "undefined") ? "No Practicals" : (numPracticals === 1) ? `${numPracticals} Practical` : `${numPracticals} Practicals`;
  };
  };

  let moduleOne = new CourseModule("Enterprise Web Development", 2,2);
  let moduleTwo = new CourseModule("Dev ops", 1); 
  const modules = [moduleOne, moduleTwo]
  return (
    <>
    <h1>{header}</h1>
    <br></br>
    <table className="table table-bordered">
    <thead>
      <tr>
        <th>Module Name</th>
        <th>Lectures</th>
        <th>Practicals</th>
      </tr>
    </thead>
    <tbody >
      <tr>
        <td>{modules[0].title}</td>
        <td>{modules[0].lectures}</td>
        <td>{modules[0].practicals}</td>
      </tr>
      <tr>
        <td>{modules[1].title}</td>
        <td>{modules[1].lectures}</td>
        <td>{modules[1].practicals}</td>
      </tr>
    </tbody >
  </table>
  </>
  );
};

export default exerciseTwo;
