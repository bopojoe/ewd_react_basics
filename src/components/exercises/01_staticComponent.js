import React from "react";
import "../../../node_modules/bootstrap/dist/css/bootstrap.css";

const exerciseOne = () => {
  return (
    <>
    <h1>MSc Enterprise Software Systems</h1>
    <table className="table table-bordered">
    <thead>
      <tr>
        <th>Module Name</th>
        <th>Lectures</th>
        <th>Practicals</th>
      </tr>
    </thead>
    <tbody >
      <tr>
        <td>Enterprise Web Development</td>
        <td>1 Lectures</td>
        <td>1 Practicals</td>
      </tr>
      <tr>
        <td>Second Module</td>
        <td>0 Lectures</td>
        <td>0 Practicals</td>
      </tr>
    </tbody >
  </table>
  </>
  );
};

export default exerciseOne;
