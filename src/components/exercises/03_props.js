import React from "react";
import "../../../node_modules/bootstrap/dist/css/bootstrap.css";


const Demo = (props) => {

  const modules = props.modules;
  
  return (
    <>
    <h1>{props.course}</h1>
    <br></br>
    <table className="table table-bordered">
    <thead>
      <tr>
        <th>Module Name</th>
        <th>Lectures</th>
        <th>Practicals</th>
      </tr>
    </thead>
    <tbody >
      <tr>
        <td>{modules[0].name}</td>
        <td>{(modules[0].noLectures === 1) ? `${modules[0].noLectures} Lectures` : `${modules[0].noLectures} Lecture`}</td>
        <td>{(modules[0].noPracticals === 1) ? `${modules[0].noPracticals} Practicals` : `${modules[0].noPracticals} Practical`}</td>
      </tr>
      <tr>
        <td>{modules[1].name}</td>
        <td>{(modules[1].noLectures === 1) ? `${modules[1].noLectures} Lectures` : `${modules[1].noLectures} Lecture`}</td>
        <td>{(modules[1].noPracticals === 1) ? `${modules[1].noPracticals} Practicals` : `${modules[1].noPracticals} Practical`}</td>
      </tr>
    </tbody >
  </table>
  </>


  );
};

export default Demo;
