import React from "react";
import "../../../node_modules/bootstrap/dist/css/bootstrap.css";


const Demo = (props) => {
  const modules = props.modules;

  const list = props.modules.map((module, index) => {
    return (
      <tr key={index}>
        <td>{module.name}</td>
        <td>{(module.noLectures === 1) ? `${module.noLectures} Lecture` : `${module.noLectures} Lectures`} </td>
        <td>{(module.noPracticals === 1) ? `${module.noPracticals} Practical` : `${module.noPracticals} Practicals`}</td>
      </tr>
    );
  });
  
  return (
    <>
    <h1>{props.course}</h1>
    <br></br>
    <table className="table table-bordered">
    <thead>
      <tr>
        <th>Module Name</th>
        <th>Lectures</th>
        <th>Practicals</th>
      </tr>
    </thead>
    <tbody >
      {list}
    </tbody >
  </table>
  </>


  );
};

export default Demo;
